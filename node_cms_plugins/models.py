from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from cms.models import CMSPlugin, Page
from django.conf import settings

header_template_choices = getattr(settings, 'NODE_CMS_PLUGIN_HEADER_TEMPLATES',(('full_size.html','Image BG Header'), ('default_hero.html','Default Header'), ('hero8-4.html', 'Title Copy Side image')))
slide_template_choices = getattr(settings, 'NODE_CMS_PLUGIN_SLIDER_TEMPLATES',(('slider_top.html','Top Level Slider'), ('slider_featured.html','Featured Slider')))
rich_link_group_template_choices = getattr(settings, 'NODE_CMS_PLUGIN_RICH_LINK_GROUP_TEMPLATES',(
    ('image_top.html','Image Top Link'),
    ('image_top_two_column.html','Image Top Link 2 Column'),
    ('image_top2.html','Image Top Link 2'),
    ('image_left.html','Image Left Link'),
    ('partner.html', 'Partners Link'),
    ('researcher.html','Researcher Link'),
    ('simple.html','Simple'),
    ('featured.html','Featured'),
))
# ---------------------------------------------------------------------------

class Slide(models.Model):
    slider = models.ForeignKey('SliderPlugin')
    title = models.CharField(max_length=255)
    content = models.TextField(max_length=80, null=True, blank=True)
    link = models.URLField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to='slides', null=True, blank=True)
    visible = models.BooleanField(default=True)

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title

class SliderPlugin(CMSPlugin):
    title = models.CharField(max_length=255)
    template = models.CharField(max_length=255, choices=slide_template_choices)

    def copy_relations(self, oldinstance):
        for slide in oldinstance.slide_set.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            slide.pk = None
            slide.slider = self
            slide.save()
# ---------------------------------------------------------------------------

class HeroImage(CMSPlugin):
    title = models.CharField(max_length=25, null=True, blank=True)
    content = models.TextField(max_length=80, null=True, blank=True)
    image = models.ImageField(upload_to='heros', null=True, blank=True)
    template = models.CharField(max_length=255, choices=header_template_choices, default='image_right.html')

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title
# ---------------------------------------------------------------------------

class FacebookLikeBox(CMSPlugin):
    """
    Facebook Like Box plugin
    """
    page_url = models.URLField()
    app_id = models.CharField(max_length=25)
    width = models.CharField(max_length=25)
    height = models.CharField(max_length=25)
    color_scheme = models.CharField(max_length=25, choices=(('light','light'),('dark','dark')), default='light')
    show_friends_faces = models.BooleanField(default=True)
    show_posts = models.BooleanField(default=True)
    show_header = models.BooleanField(default=True)
    show_border = models.BooleanField(default=True)
# ---------------------------------------------------------------------------

class Promotion(CMSPlugin):
    """
    Promotion plugin
    """
    title = models.CharField(max_length=20, null=True, blank=True)
    intro = models.TextField(max_length=80, null=True, blank=True)
    image = models.ImageField(upload_to='promotions', null=True, blank=True)
    promo_link  = models.URLField(blank=True)
# ---------------------------------------------------------------------------

class Modal(CMSPlugin):
    active = models.BooleanField(default=False)
    show_once = models.BooleanField(default=True)
    title = models.CharField(max_length=25, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to='modal')
    url = models.URLField(null=True, blank=True)
    page = models.ForeignKey(Page, null=True, blank=True)
    button_text = models.CharField(max_length=25, null=True, default='Find out more')
    button_color = models.CharField(max_length=25, choices=(('blue','blue'),('purple','purple'),('pink','pink')), default='purple')
    content = HTMLField()

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title
# ---------------------------------------------------------------------------

class TextList(CMSPlugin):
    """
    TextList plugin
    """
    title = models.CharField(max_length=80, null=True, blank=True)
    content = HTMLField()

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title
# ---------------------------------------------------------------------------

class RedirectAction(CMSPlugin):
    url = models.CharField(max_length=255, null=True, blank=True)
    page = models.ForeignKey(Page, null=True, blank=True)

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.url
# ---------------------------------------------------------------------------

class RichLink(models.Model):
    group = models.ForeignKey('RichLinkGroup')
    order = models.IntegerField(default=0)
    title = models.CharField(max_length=255, null=True, blank=True)
    content = models.TextField(max_length=25, null=True, blank=True)
    button_text = models.CharField(max_length=25, null=True, default='Find out more')
    button_color = models.CharField(max_length=25, choices=(('blue','blue'),('purple','purple'),('pink','pink')), default='purple')
    link_color = models.CharField(max_length=25,  choices=(('none','none'),('blue','blue'),('purple','purple'),('pink','pink')), default='none')
    url = models.URLField(max_length=255, null=True, blank=True)
    page = models.ForeignKey(Page, null=True, blank=True)
    image = models.ImageField(upload_to='slides', null=True, blank=True)
    visible = models.BooleanField(default=True)

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title

    class Meta:
        ordering=['order']
# ---------------------------------------------------------------------------

class RichLinkGroup(CMSPlugin):
    title = models.CharField(max_length=255)
    template = models.CharField(max_length=255, choices=rich_link_group_template_choices)

    def copy_relations(self, oldinstance):
        for link in oldinstance.richlink_set.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            link.pk = None
            link.group = self
            link.save()

# ---------------------------------------------------------------------------

class RichFile(CMSPlugin):

    title = models.CharField(max_length=255)
    rich_file = models.FileField(upload_to='richfiles/files')
    image = models.ImageField(upload_to='richfiles/images', null=True, blank=True)
    show_image = models.BooleanField(default=True)
    show_title = models.BooleanField(default=True)

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title


