from django.shortcuts import redirect

class RedirectActionMiddleware(object):
    #def process_template_response(self, request, response):
    def process_response(self, request, response):
        if hasattr(request, 'cms_redirect_action'):
            return redirect(request.cms_redirect_action)
        return response
