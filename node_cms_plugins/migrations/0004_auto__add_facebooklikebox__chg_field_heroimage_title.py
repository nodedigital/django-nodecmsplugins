# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FacebookLikeBox'
        db.create_table(u'cmsplugin_facebooklikebox', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('page_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('app_id', self.gf('django.db.models.fields.TextField')(max_length=25)),
            ('width', self.gf('django.db.models.fields.TextField')(max_length=25)),
            ('height', self.gf('django.db.models.fields.TextField')(max_length=25)),
            ('color_scheme', self.gf('django.db.models.fields.TextField')(max_length=25)),
            ('show_friends_faces', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('show_posts', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('show_header', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('show_border', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'node_cms_plugins', ['FacebookLikeBox'])


        # Changing field 'HeroImage.title'
        db.alter_column(u'cmsplugin_heroimage', 'title', self.gf('django.db.models.fields.CharField')(max_length=20, null=True))

    def backwards(self, orm):
        # Deleting model 'FacebookLikeBox'
        db.delete_table(u'cmsplugin_facebooklikebox')


        # User chose to not deal with backwards NULL issues for 'HeroImage.title'
        raise RuntimeError("Cannot reverse this migration. 'HeroImage.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'HeroImage.title'
        db.alter_column(u'cmsplugin_heroimage', 'title', self.gf('django.db.models.fields.CharField')(max_length=20))

    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'node_cms_plugins.facebooklikebox': {
            'Meta': {'object_name': 'FacebookLikeBox', 'db_table': "u'cmsplugin_facebooklikebox'", '_ormbases': ['cms.CMSPlugin']},
            'app_id': ('django.db.models.fields.TextField', [], {'max_length': '25'}),
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'color_scheme': ('django.db.models.fields.TextField', [], {'max_length': '25'}),
            'height': ('django.db.models.fields.TextField', [], {'max_length': '25'}),
            'page_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'show_border': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_friends_faces': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_header': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_posts': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'width': ('django.db.models.fields.TextField', [], {'max_length': '25'})
        },
        u'node_cms_plugins.heroimage': {
            'Meta': {'object_name': 'HeroImage', 'db_table': "u'cmsplugin_heroimage'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'max_length': '80', 'null': 'True', 'blank': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'node_cms_plugins.slide': {
            'Meta': {'object_name': 'Slide'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '80', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '255'}),
            'slider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['node_cms_plugins.SliderPlugin']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'node_cms_plugins.sliderplugin': {
            'Meta': {'object_name': 'SliderPlugin', 'db_table': "u'cmsplugin_sliderplugin'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['node_cms_plugins']