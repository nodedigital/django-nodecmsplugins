from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import *
from home.forms import *
from event.models import Event
from news.models import NewsItem
from research.models import Researcher
from django.contrib import admin
from django.conf import settings

# Slider plugin
# --------------------------------------------------

class SlideInline(admin.StackedInline):
    model = Slide
    extra = 0

class CMSSliderPlugin(CMSPluginBase):
    model = SliderPlugin
    name = "Slider"
    inlines = [SlideInline]

    def render(self, context, instance, placeholder):
        self.render_template = "slider/{}".format(instance.template)
        context.update({
            'slides': instance.slide_set.filter(visible=True),
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSSliderPlugin)

# Picture plugin
# --------------------------------------------------

class CMSHeroImagePlugin(CMSPluginBase):
    model = HeroImage
    name = "Hero Image"

    def render(self, context, instance, placeholder):

        self.render_template = "hero_image/{}".format(instance.template)
        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSHeroImagePlugin)

# Facebook Like Box plugin
# --------------------------------------------------

class CMSFacebookLikeBoxPlugin(CMSPluginBase):
    model = FacebookLikeBox
    name = "Facebook Like Box"
    render_template = "facebooklikebox/default.html"

    def render(self, context, instance, placeholder):

        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSFacebookLikeBoxPlugin)

# Promotion plugin
# !*!*!*!*!*! DISABLED !*!*!*!*!*!
# --------------------------------------------------

class CMSPromotionPlugin(CMSPluginBase):
    model = Promotion
    name = "Promotion"
    render_template = "promotion/default_promotion.html"

    def render(self, context, instance, placeholder):

        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

# plugin_pool.register_plugin(CMSPromotionPlugin)


class CMSModalPlugin(CMSPluginBase):
    model = Modal
    render_template = "modal/default.html"

    def render(self, context, instance, placeholder):

        session = context['request'].session

        modal_viewed = session.get('modal_viewed',[])

        if instance.show_once:
            # if modal already viewed, then it's in the modal_viewed
            if instance.id in modal_viewed:
                instance.active = False

        if not instance.id in modal_viewed:
            modal_viewed.append(instance.id)
        session['modal_viewed'] = modal_viewed
        session.modified = True

        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSModalPlugin)

class CMSTextList(CMSPluginBase):
    model = TextList
    name = "Text List"
    render_template = "text_list/default.html"

    def render(self, context, instance, placeholder):

        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSTextList)

class CMSRedirectActionPlugin(CMSPluginBase):
    model = RedirectAction
    render_template = "redirectaction/default.html"
    print '*' * 100

    def render(self, context, instance, placeholder):

        if instance.url:
            context['request'].cms_redirect_action = instance.url
        else:
            context['request'].cms_redirect_action = instance.page.get_absolute_url()
        return context

plugin_pool.register_plugin(CMSRedirectActionPlugin)

# RichLink plugin
# --------------------------------------------------

class RichLinkInline(admin.StackedInline):
    model = RichLink
    extra = 0

class CMSRichLinkGroupPlugin(CMSPluginBase):
    model = RichLinkGroup
    name = "Rich Link Group"
    inlines = [RichLinkInline]

    def render(self, context, instance, placeholder):
        self.render_template = "rich_link/{}".format(instance.template)
        context.update({
            'rich_links': instance.richlink_set.filter(visible=True).order_by('order', 'title'),
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSRichLinkGroupPlugin)

class CMSFeaturedPlugin(CMSPluginBase):

    name = "Featured Items"
    render_template = "featured_plugin/home_featured.html"

    def get_or_first(self, model, **kwargs):
        try:
            return model.objects.filter(**kwargs)[0]
        except model.DoesNotExist:
            return model.objects.all()[0]
        except:
            return None

    def render(self, context, instance, placeholder):

        event = self.get_or_first(Event, featured=True)
        news = self.get_or_first(NewsItem, featured=True)
        researcher = self.get_or_first(Researcher, featured=True)
        context.update({
            'object': instance,
            'placeholder': placeholder,
            'featured_event': event ,
            'featured_news': news,
            'featured_researcher': researcher
        })
        return context

plugin_pool.register_plugin(CMSFeaturedPlugin)

class CMSRichFilePlugin(CMSPluginBase):
    model = RichFile
    name = "Rich File Plugin"

    def render(self, context, instance, placeholder):
        self.render_template = "rich_file/default.html"
        context.update({
            'object': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSRichFilePlugin)

# --------------------------------------------------

class CMSContactFormPlugin(CMSPluginBase):

    name = 'Contact Form'
    render_template = "forms/contact.html"

    def render(self, context, instance, placeholder):

        form = ContactForm()
        request = context['request']

        if request.META['REQUEST_METHOD'] != 'POST':
            context.update({
                'form': form,
                'object': instance,
                'placeholder': placeholder
            })
            return context
        else:

            POST = request.POST
            form = ContactForm(POST)

            context.update({
                'form': form,
                'object': instance,
                'placeholder': placeholder

                })
            if form.is_valid():
                # Send mail
                from django.core.mail import send_mail

                subject_choice = int(request.POST['subject_choice'])
                recipient = [settings.CONTACT_CHOICES[subject_choice]]
                name = request.POST['name']
                sender = '{} <{}>'.format(name, request.POST['email'])
                message = request.POST['message']
                subject = contact_choices[subject_choice][1]
                form.sent = True
                send_mail(subject, message, sender, recipient)

            return context
plugin_pool.register_plugin(CMSContactFormPlugin)
