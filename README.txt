=====
Node CMS Plugins
=====

Polls is a simple Django app to conduct Web-based polls. For each
question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "node_cms_plugins" to your INSTALLED_APPS setting like this::

      INSTALLED_APPS = (
          ...
          'node_cms_plugins',
      )

3. Run `python manage.py syncdb` to create the node_cms_plugins models or Run 'python manage.py migrate node_cms_plugins' if you have south installed

4. Start the development server and visit http://127.0.0.1:8000/admin/ to create an article (you'll need the Admin app enabled).
